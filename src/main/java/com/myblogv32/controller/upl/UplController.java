package com.myblogv32.controller.upl;

import com.myblogv32.service.upl.UplService;
import com.myblogv32.util.common.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/upl")
public class UplController {

    @Autowired
    private UplService uplService;

    @RequestMapping("/uplCreate")
    public Map<String,Object> uplCreate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uplService.createUpl(paramMap);
        Map<String,Object> resultMap = new HashMap<>();

        if(result >= 1 )
        {
            resultMap.put("uplIdx",paramMap.get("uplIdx"));
            return resultMap;
        }
        return resultMap;
    }

    @RequestMapping("/uplSelect")
    public List<Map<String,Object>> uplSelect(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        List<Map<String,Object>> resultMap = uplService.selectUpl(paramMap);
        return resultMap;
    }

    @RequestMapping("/uplSelectCnt")
    public int uplSelectCnt(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        return uplService.selectUplCount(paramMap);
    }

    @RequestMapping("/uplUpdate")
    public Boolean uplUpdate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uplService.updateUpl(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @RequestMapping("/uplUpdateVcnt")
    public Boolean uplUpdateVcnt(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uplService.updattVcnt(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @RequestMapping("/uplDelete")
    public Boolean uplDelete(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uplService.deleteUpl(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
