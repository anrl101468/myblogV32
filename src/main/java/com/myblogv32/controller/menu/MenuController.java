package com.myblogv32.controller.menu;

import com.myblogv32.service.menu.MenuService;
import com.myblogv32.util.common.RequestParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/menuCreate")
    public Map<String,Object> menuCreate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = menuService.createMenu(paramMap);
        paramMap.remove("usrVo");

        if(result > 0)
        {
            logger.info("결과 = "+result + "paramMap = " + paramMap );
        }
        return paramMap;
    }

    @RequestMapping("/menuSelect")
    public List<Map<String, Object>> MenuSelect(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        List<Map<String, Object>> menuMap = null;
        if(paramMap.get("usrVo") != null || paramMap.get("usrId") != null)
        {
            menuMap = menuService.selectMenuList(paramMap);
        }
        return menuMap;
    }

    @RequestMapping("/menuUpdate")
    public boolean MenuUpdate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = menuService.updateMenu(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    @RequestMapping("/menuDelete")
    public boolean MenuDelete(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = menuService.deleteMenu(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
