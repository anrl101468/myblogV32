package com.myblogv32.controller.pv;

import com.myblogv32.service.pvCnt.PvCntService;
import com.myblogv32.util.common.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/pv")
public class PvController {

    @Autowired
    private PvCntService pvCntService;

    @RequestMapping("/viewAdd")
    public int postingViewAdd(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        return pvCntService.createPvCnt(paramMap);
    }
}
