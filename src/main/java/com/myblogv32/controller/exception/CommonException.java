/*
package com.myblogv32.controller.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CommonException {

    final Logger logger =LoggerFactory.getLogger(CommonException.class);

    @Autowired
    private HttpServletRequest request;
    // 기본 애러 메세지를 나타내는 메소드
    @ExceptionHandler(Exception.class)
    private String basicException(Exception e)
    {
        logger.error("========================================================================");
        logger.error("접속 아이피:"+request.getRemoteAddr());
        logger.error(e.getMessage());
        logger.error("========================================================================");
        return e.getMessage();
    }
}
*/
