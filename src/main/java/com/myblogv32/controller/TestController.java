package com.myblogv32.controller;

import com.myblogv32.service.test.TestService;
import com.myblogv32.service.usr.UsrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@RestController
public class TestController {

    @RequestMapping("/api/test")
    public String hello(Map<String,Object> paramMap) throws Exception {

        return "main";
    }

}
