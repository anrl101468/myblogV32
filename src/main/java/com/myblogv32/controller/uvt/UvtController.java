package com.myblogv32.controller.uvt;

import com.myblogv32.service.uvtCnt.UvtCntService;
import com.myblogv32.util.StringUtil;
import com.myblogv32.util.common.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/uvt")
public class UvtController {

    @Autowired
    private UvtCntService uvtCntService;

    @RequestMapping("/uvtAdd")
    public int blogViewAdd(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        return uvtCntService.createUvtCnt(paramMap);
    }

    @RequestMapping("/uvtGetViews")
    public Map<String,Object> blogGetViews(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        List<Map<String, Object>> itemList = uvtCntService.selectUvtCnt(paramMap);
        ArrayList categoryItem = new ArrayList();
        ArrayList chartItem = new ArrayList();
        Calendar cal = Calendar.getInstance();  // 캘린더 인스턴스 생성
        cal.set(Calendar.YEAR, StringUtil.getInt(paramMap.get("year")));
        cal.set(Calendar.MONTH,StringUtil.getInt(paramMap.get("month"))-1);               // 현재 날짜 설정
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);  // 해당 달의 마지막 날짜 구하기

        for(int i= 1; i< lastDay+1; i++)
        {
            categoryItem.add(i);
        }

        for(int i=0;i <lastDay; i++)
        {
            if(itemList.size() > 0)
            {
                for(int y=0; y<itemList.size(); y++)
                {
                   int tempDt = Integer.parseInt(itemList.get(y).get("uvtDt").toString().substring(8,10)) -1;
                   if(i == tempDt)
                   {
                       chartItem.add(Integer.parseInt(itemList.get(y).get("uvtCnt").toString()));
                       break;
                   } else if (y == itemList.size()-1) {
                       chartItem.add(0);
                   }
                }
            }
            else
            {
                chartItem.add(0);
            }
        }
        Map<String,Object> returnMap = new HashMap<String,Object>();
        returnMap.put("categoryItem",categoryItem);
        returnMap.put("chartItem",chartItem);
        return returnMap;

    }

}
