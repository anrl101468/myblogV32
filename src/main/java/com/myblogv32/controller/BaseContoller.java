package com.myblogv32.controller;

import com.myblogv32.service.conHis.ConHisService;
import com.myblogv32.util.common.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class BaseContoller {

    @Autowired
    private ConHisService conHisService;

    @RequestMapping("/connect")
    public void connect(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        conHisService.createConHis(paramMap);
    }
}
