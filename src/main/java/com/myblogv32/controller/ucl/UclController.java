package com.myblogv32.controller.ucl;

import com.myblogv32.service.ucl.UclService;
import com.myblogv32.util.common.RequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ucl")
public class UclController {

    @Autowired
    private UclService uclService;

    @RequestMapping("/uclCreate")
    public Map<String,Object> uclCreate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uclService.createUcl(paramMap);
        Map<String,Object> resultMap = new HashMap<>();

        if(result >= 1 )
        {
            resultMap.put("uclIdx",paramMap.get("uclIdx"));
            return resultMap;
        }
        return resultMap;
    }

    @RequestMapping("/uclSelect")
    public List<Map<String,Object>> uclSelect(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        List<Map<String,Object>> resultMap = uclService.selectUcl(paramMap);
        return resultMap;
    }

    @RequestMapping("/uclUpdate")
    public Boolean uclUpdate(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uclService.updateUcl(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @RequestMapping("/uclDelete")
    public Boolean uplDelete(@RequestParams Map<String,Object> paramMap) throws Exception
    {
        int result = uclService.deleteUcl(paramMap);
        if(result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
