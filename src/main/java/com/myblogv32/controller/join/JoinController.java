package com.myblogv32.controller.join;

import com.myblogv32.config.token.JwtTokenProvider;
import com.myblogv32.service.refreshToken.RefreshTokenService;
import com.myblogv32.service.usr.UsrService;
import com.myblogv32.util.TransUtil;
import com.myblogv32.util.common.RequestParams;
import com.myblogv32.vo.UsrVo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static com.myblogv32.util.DateUtil.getLocalDateTime;

/**
 * @PackageName: JoinController
 * @FileName : JoinController.java
 * @Date : 2023-04-22 오후 1:32
 * @프로그램 설명 : 회원 가입 컨트롤러
 * @author : ksy
 */
@RestController
@RequestMapping("/api")
public class JoinController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UsrService usrService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private RefreshTokenService refreshTokenService;
    @RequestMapping("/joinProc")
    public Map<String,Object> JoinMember(HttpServletRequest request, HttpServletResponse response, @RequestParams Map<String,Object> paramMap) throws Exception
    {
        UsrVo usr = new UsrVo(paramMap.get("usrId").toString(),paramMap.get("usrName").toString(),bCryptPasswordEncoder.encode(paramMap.get("usrPassword").toString()));
        usr.setUsrRecentDt(getLocalDateTime());
        usr.setUsrRecentIp(paramMap.get("ACCESS_IP").toString());
        usr.setUsrRegDt(getLocalDateTime());
        usr.setUsrRegIp(paramMap.get("ACCESS_IP").toString());
        usr.setUsrPwFailCnt(0);
        usr.setUsrRole("user");

        Map<String,Object> usrMap = TransUtil.toMap(usr);
        int i = usrService.createUsr(usrMap);

        usrMap = usrService.selectUsr(usrMap);
        UsrVo tokenUsrVo = TransUtil.toVo(usrMap,UsrVo.class);
        String newJwtToken = jwtTokenProvider.createAcessToken(tokenUsrVo);
        String jwtRefreshToken = jwtTokenProvider.createRefreshToken(tokenUsrVo);
        Map<String,Object> tokenMap = new HashMap<>();
        tokenMap.put("usrVo",tokenUsrVo);
        tokenMap.put("token",jwtRefreshToken);
        try {
            int tokenIdx = refreshTokenService.createToken(tokenMap);
            response.setHeader("tokenIdx", tokenMap.get("tokenIdx").toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        response.setHeader("Authorization", "Bearer " + newJwtToken);

        // map의 회원 이름,아이디,jwtToken 전송
        return usrMap;
    }
}
