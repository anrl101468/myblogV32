package com.myblogv32.controller;

import com.myblogv32.config.token.JwtTokenProvider;
import com.myblogv32.service.usr.UsrService;
import com.myblogv32.util.StringUtil;
import com.myblogv32.util.TransUtil;
import com.myblogv32.util.common.RequestParams;
import com.myblogv32.vo.UsrVo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.json.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.Map;

@RestController
@RequestMapping("/api/usr")
public class UsrController {

    @Autowired
    private UsrService usrService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @RequestMapping("/getUsrInfo")
    public UsrVo getUsrInfo(@RequestParams Map<String,Object> paramMap) throws Exception
    {

        UsrVo usrVo = (UsrVo) paramMap.get("usrVo");
        if(usrVo != null)
        {
            usrVo.setUsrPassword(null);
            return usrVo;
        }
        return null;
    }

    @RequestMapping("/modifyUsr")
    public int modifyUsr(@RequestParams Map<String, Object> paramMap) throws Exception
    {
        int result = usrService.updateUsr(paramMap);
        return result;
    }

    @RequestMapping(value = "/duplicateCheck")
    public String duplicateCheck(HttpServletRequest request , HttpServletResponse response, @RequestParams Map<String, Object> paramMap) throws Exception
    {
        Map<String,Object> map = usrService.selectUsr(paramMap);
        String returnValue = map == null ? "true" : "false";
        return returnValue;
        //return map == null;
    }


}
