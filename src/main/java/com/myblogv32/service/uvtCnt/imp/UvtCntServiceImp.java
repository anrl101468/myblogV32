package com.myblogv32.service.uvtCnt.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.uvtCnt.UvtCntService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UvtCntServiceImp implements UvtCntService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public int createUvtCnt(Map<String, Object> paramMap) throws Exception {
        paramMap.put("uvtDt",paramMap.get("ACCESS_DT").toString().substring(0,10));
        return dao.insert("uvtCnt.createUvtCnt",paramMap);
    }

    @Override
    public List<Map<String, Object>> selectUvtCnt(Map<String, Object> paramMap) throws Exception {
        return dao.selectList("uvtCnt.selectUvtCnt",paramMap);
    }
}
