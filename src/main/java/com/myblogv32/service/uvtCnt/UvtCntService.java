package com.myblogv32.service.uvtCnt;

import java.util.List;
import java.util.Map;

public interface UvtCntService {

    public int createUvtCnt(Map<String,Object> paramMap) throws Exception;

    public List<Map<String, Object>> selectUvtCnt(Map<String,Object> paramMap) throws Exception;

}
