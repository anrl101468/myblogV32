package com.myblogv32.service.ucl.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.ucl.UclService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UclServiceImp implements UclService {
    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public List<Map<String, Object>> selectUcl(Map<String, Object> paramMap) throws Exception {
        return dao.selectList("ucl.selectUcl",paramMap);
    }

    @Override
    public int createUcl(Map<String, Object> paramMap) throws Exception {
        // 기본 정보 세팅
        paramMap.put("uclRegDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uclRegIp",paramMap.get("ACCESS_IP"));
        paramMap.put("uclModDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uclModIp",paramMap.get("ACCESS_IP"));
        paramMap.put("uclIdx",null);

        return dao.insert("ucl.createUcl",paramMap);
    }

    @Override
    public int updateUcl(Map<String, Object> paramMap) throws Exception {
        paramMap.put("uclModDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uclModIp",paramMap.get("ACCESS_IP"));
        return dao.update("ucl.updateUcl",paramMap);
    }

    @Override
    public int deleteUcl(Map<String, Object> paramMap) throws Exception {
        return dao.delete("ucl.deleteUcl",paramMap);
    }

    @Override
    public int selectUclCount(Map<String, Object> paramMap) throws Exception {
        return dao.selectCount("upl.selectUclCount",paramMap);
    }
}
