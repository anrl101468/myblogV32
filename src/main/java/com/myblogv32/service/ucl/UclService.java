package com.myblogv32.service.ucl;

import java.util.List;
import java.util.Map;

public interface UclService {

    public List<Map<String,Object>> selectUcl(Map<String,Object> paramMap) throws Exception;

    public int createUcl(Map<String,Object> paramMap) throws Exception;

    public int updateUcl(Map<String,Object> paramMap) throws Exception;

    public int deleteUcl(Map<String,Object> paramMap) throws Exception;

    public int selectUclCount(Map<String,Object> paramMap) throws Exception;
}
