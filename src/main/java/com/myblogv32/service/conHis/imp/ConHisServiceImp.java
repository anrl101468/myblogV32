package com.myblogv32.service.conHis.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.conHis.ConHisService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ConHisServiceImp implements ConHisService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;
    
    @Override
    public int createConHis(Map<String, Object> paramMap) throws Exception {
        // 접속 history
        return dao.insert("conHis.createConHis",paramMap);
    }
}
