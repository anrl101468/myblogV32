package com.myblogv32.service.menu.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.menu.MenuService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MenuServiceImp implements MenuService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public int createMenu(Map<String, Object> paramMap) throws Exception {
        paramMap.put("menuRegIp",paramMap.get("ACCESS_IP"));
        paramMap.put("menuRegDt",paramMap.get("ACCESS_DT"));
        paramMap.put("menuModIp",paramMap.get("ACCESS_IP"));
        paramMap.put("menuModDt",paramMap.get("ACCESS_DT"));
        paramMap.putIfAbsent("menuUpIdx", "0");
        return dao.insert("menu.createMenu",paramMap);
    }

    @Override
    public List<Map<String, Object>> selectMenuList(Map<String, Object> paramMap) throws Exception {
        paramMap.putIfAbsent("menuUpIdx", "0"); // menuUpIdx 없으면 0을 저장
        return dao.selectList("menu.selectMenu",paramMap);
    }

    @Override
    public int updateMenu(Map<String, Object> paramMap) throws Exception {
        paramMap.put("menuModIp",paramMap.get("ACCESS_IP"));
        paramMap.put("menuModDt",paramMap.get("ACCESS_DT"));
        return dao.update("menu.updateMenu",paramMap);
    }

    @Override
    public int deleteMenu(Map<String, Object> paramMap) throws Exception {
        return dao.delete("menu.deleteMenu",paramMap);
    }

    @Override
    public int countMenu(Map<String, Object> paramMap) throws Exception {
        return dao.selectCount("menu.countMenu",paramMap);
    }
}
