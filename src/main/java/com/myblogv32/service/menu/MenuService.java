package com.myblogv32.service.menu;

import java.util.List;
import java.util.Map;

public interface MenuService {

    public int createMenu(Map<String,Object> paramMap) throws Exception;

    public List<Map<String, Object>> selectMenuList(Map<String, Object> paramMap) throws Exception;

    public int updateMenu(Map<String,Object> paramMap) throws Exception;

    public int deleteMenu(Map<String,Object> paramMap) throws Exception;

    public int countMenu(Map<String,Object> paramMap) throws Exception;
}
