package com.myblogv32.service.test;

import java.util.List;
import java.util.Map;

public interface TestService {

    public List<Map<String,Object>> selectTest(Map<String,Object> paramMap) throws Exception;

}
