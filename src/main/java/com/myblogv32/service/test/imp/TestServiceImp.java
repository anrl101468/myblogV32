package com.myblogv32.service.test.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.test.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TestServiceImp implements TestService {

    @Autowired
    CommonDefaultDAO cDD;
    @Override
    public List<Map<String, Object>> selectTest(Map<String, Object> paramMap) throws Exception {
        return cDD.selectList("test.selectList",paramMap);
    }
}
