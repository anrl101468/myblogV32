package com.myblogv32.service.refreshToken;

import com.myblogv32.dao.CommonDefaultDAO;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class RefreshTokenServiceImp implements RefreshTokenService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public int createToken(Map<String, Object> paramMap) throws Exception {
        return dao.insert("refreshToken.createToken",paramMap);
    }

    @Override
    public Map<String, Object> selectToken(Map<String, Object> paramMap) throws Exception{
        return dao.select("refreshToken.selectToken",paramMap);
    }
}
