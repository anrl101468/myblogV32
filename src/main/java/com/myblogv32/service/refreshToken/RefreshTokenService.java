package com.myblogv32.service.refreshToken;

import java.util.Map;

public interface RefreshTokenService {

    public int createToken(Map<String,Object> paramMap) throws Exception;

    public Map<String,Object> selectToken(Map<String,Object> paramMap) throws Exception;
}
