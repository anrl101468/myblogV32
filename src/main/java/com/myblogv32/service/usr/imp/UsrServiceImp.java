package com.myblogv32.service.usr.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.usr.UsrService;
import com.myblogv32.vo.UsrVo;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UsrServiceImp implements UsrService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO defaultDAO;
    @Override
    public int createUsr(Map<String,Object> paramMap) throws Exception {
        return defaultDAO.insert("usr.insertUsr",paramMap);
    }

    public Map<String,Object> selectUsr(Map<String,Object> paramMap) throws Exception {
        return defaultDAO.select("usr.selectUsr",paramMap);
    }

    public int updateUsr(Map<String,Object> paramMap) throws Exception {
        UsrVo usrVo = (UsrVo) paramMap.get("usrVo");
        if(paramMap.get("usrIdx") == null)
        {
            paramMap.put("usrIdx",usrVo.getUsrIdx());
        }
        return defaultDAO.update("usr.updateUsr",paramMap);
    }

    public int deleteUsr(Map<String,Object> paramMap) throws Exception {
        return defaultDAO.delete("usr.deleteUsr",paramMap);
    }

    public int selectIdAndPassword(Map<String,Object> paramMap) throws Exception {
        return defaultDAO.selectCount("usr.passwordCehck",paramMap);
    }
}
