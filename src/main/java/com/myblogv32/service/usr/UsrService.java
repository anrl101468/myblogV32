package com.myblogv32.service.usr;

import java.util.Map;

public interface UsrService {

    public int createUsr(Map<String,Object> paramMap) throws Exception;

    public Map<String,Object> selectUsr(Map<String,Object> paramMap) throws Exception;

    public int deleteUsr(Map<String,Object> paramMap) throws Exception;

    public int updateUsr(Map<String,Object> paramMap) throws Exception;

    public int selectIdAndPassword(Map<String,Object> paramMap) throws Exception;
}
