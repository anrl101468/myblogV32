package com.myblogv32.service.upl;

import java.util.List;
import java.util.Map;

public interface UplService {

    public List<Map<String,Object>> selectUpl(Map<String,Object> paramMap) throws Exception;

    public int selectUplCount(Map<String,Object> paramMap) throws Exception;

    public int createUpl(Map<String,Object> paramMap) throws Exception;

    public int updateUpl(Map<String,Object> paramMap) throws Exception;

    public int deleteUpl(Map<String,Object> paramMap) throws Exception;

    public int updattVcnt(Map<String,Object> paramMap) throws Exception;

}
