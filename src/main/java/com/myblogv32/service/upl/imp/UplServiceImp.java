package com.myblogv32.service.upl.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.upl.UplService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class UplServiceImp implements UplService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public List<Map<String, Object>> selectUpl(Map<String, Object> paramMap) throws Exception {

        if(paramMap.get("page") != null)
        {
            int page = Integer.parseInt((String) paramMap.get("page"));
            int rowsPerPage = Integer.parseInt((String) paramMap.get("rowsPerPage"));
            int start = page * rowsPerPage;

            paramMap.put("start",start);
            paramMap.put("rowsPerPage",rowsPerPage);
        }

        return dao.selectList("upl.selectUpl",paramMap);
    }

    @Override
    public int selectUplCount(Map<String, Object> paramMap) throws Exception {
        return dao.selectCount("upl.selectUplCount",paramMap);
    }

    @Override
    public int createUpl(Map<String, Object> paramMap) throws Exception {
        // 기본 정보 세팅
        paramMap.put("uplRegDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uplRegIp",paramMap.get("ACCESS_IP"));
        paramMap.put("uplModDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uplModIp",paramMap.get("ACCESS_IP"));
        paramMap.put("uplIdx",null);

        return dao.insert("upl.createUpl",paramMap);
    }

    @Override
    public int updateUpl(Map<String, Object> paramMap) throws Exception {
        paramMap.put("uplModDt",paramMap.get("ACCESS_DT"));
        paramMap.put("uplModIp",paramMap.get("ACCESS_IP"));
        return dao.update("upl.updateUpl",paramMap);
    }

    @Override
    public int updattVcnt(Map<String, Object> paramMap) throws Exception {
        return dao.update("upl.updateVcnt",paramMap);
    }

    @Override
    public int deleteUpl(Map<String, Object> paramMap) throws Exception {
        return dao.delete("upl.deleteUpl",paramMap);
    }


}
