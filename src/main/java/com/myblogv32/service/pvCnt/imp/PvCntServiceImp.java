package com.myblogv32.service.pvCnt.imp;

import com.myblogv32.dao.CommonDefaultDAO;
import com.myblogv32.service.pvCnt.PvCntService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PvCntServiceImp implements PvCntService {

    @Resource(name="commonDefaultDAO")
    private CommonDefaultDAO dao;

    @Override
    public int createPvCnt(Map<String, Object> paramMap) throws Exception {
        paramMap.put("pvDt",paramMap.get("ACCESS_DT").toString().substring(0,10));
        return dao.insert("pvCnt.createPvCnt",paramMap);
    }
}
