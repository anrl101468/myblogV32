package com.myblogv32.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myblogv32.util.StringUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
public class CommonDefaultDAO implements DefaultIntefaceDao {
    @Autowired
    @Qualifier("writeSqlSession")
    private SqlSession writeSqlSession;

    @Autowired
    @Qualifier("readSqlSession")
    private SqlSession readSqlSession;

    /**
     * 데이터 등록한다.
     *
     * @param param - 등록할 정보가 담긴 param
     * @return 등록 결과
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public int insert(String queryID, Map param) throws Exception {
        return (Integer) StringUtil.getInt(writeSqlSession.insert(queryID, (Object) param), 1);
    }

    @SuppressWarnings("rawtypes")
    public int insertSeqSelect(String queryID, Map param) throws Exception {
        return writeSqlSession.insert(queryID, (Object) param);
    }

    @SuppressWarnings("rawtypes")
    public int insertNoException(String queryID, Map param) throws Exception {
        int result = 400;
        try {
            result = writeSqlSession.insert(queryID, (Object) param);
        } catch (Exception ex) {
            ex.printStackTrace();
            return result;
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    public String insertStr(String queryID, Map param) throws Exception {
        return StringUtil.getString(writeSqlSession.insert(queryID, (Object) param), "");
    }

    @SuppressWarnings("rawtypes")
    public void insertNoReturn(String queryID, Map param) throws Exception {
        writeSqlSession.insert(queryID, (Object) param);
    }

    @SuppressWarnings("rawtypes")
    public int insertObject(String queryID, Map param) throws Exception {
        int result = 400;
        try {
            result = writeSqlSession.insert(queryID, (Object) param);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * 데이터 수정한다.
     *
     * @param param - 수정할 정보가 담긴 param
     * @return 삭제 결과
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public int update(String queryID, Map param) throws Exception {
        return (int) writeSqlSession.update(queryID, (Object) param);
    }

    /**
     * 데이터 삭제한다.
     *
     * @param param - 삭제할 정보가 담긴 param
     * @return 삭제 결과
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public int delete(String queryID, Map param) throws Exception {
        return (int) writeSqlSession.delete(queryID, (Object) param);
    }

    /**
     * 데이터 삭제한다.
     *
     * @param param - 삭제할 정보가 담긴 param
     * @return 삭제 결과
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public int deleteNoException(String queryID, Map param) throws Exception {
        return (int) writeSqlSession.delete(queryID, (Object) param);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Map<String, Object> select(String queryID, Map param) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
//    mapper.convertValue(readSqlSession.selectOne(queryID, param), Map.class);
        return mapper.convertValue(readSqlSession.selectOne(queryID, param), Map.class);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Map<String, Object> selectWithException(String queryID, Map param) throws Exception {
        return (Map<String, Object>) readSqlSession.selectOne(queryID, param);
    }

    public Object select(String queryID, Object param) throws Exception {
        return (Object) readSqlSession.selectOne(queryID, param);
    }

    public String selectStr(String queryID, Object param) throws Exception {
        return (String) readSqlSession.selectOne(queryID, param);
    }

    public String selectWriteStr(String queryID, Object param) throws Exception {
        return (String) writeSqlSession.selectOne(queryID, param);
    }

    /**
     * 데이터 목록을 조회한다.
     *
     * @param param - 조회할 정보가 담긴 param
     * @return 글 목록
     * @throws Exception
     */
    public List<Map<String, Object>> selectList(String queryID, Map<String, Object> param) throws Exception {
        List<Map<String, Object>> returnList = readSqlSession.selectList(queryID, param);
        if (returnList == null) {
            returnList = new ArrayList<Map<String, Object>>();
        }
        return returnList;
    }

    public List<Map<String, Object>> selectList(String queryID, String param) throws Exception {
        List<Map<String, Object>> returnList = readSqlSession.selectList(queryID, param);
        if (returnList == null) {
            returnList = new ArrayList<Map<String, Object>>();
        }
        return returnList;
    }

    public List<Map<String, Object>> selectList(String queryID, int param) throws Exception {
        List<Map<String, Object>> returnList = readSqlSession.selectList(queryID, param);
        if (returnList == null) {
            returnList = new ArrayList<Map<String, Object>>();
        }
        return returnList;
    }


    /**
     * @param param - 조회할 정보가 담긴 param
     * @return 글 총 갯수
     * @throws Exception 데이터 총 갯수를 조회한다.
     * @throws
     */
    @SuppressWarnings("rawtypes")
    public int selectCount(String queryID, Map param) throws Exception {
        return (Integer) readSqlSession.selectOne(queryID, param);
    }

    @SuppressWarnings("rawtypes")
    public int selectWriteCount(String queryID, Map param) throws Exception {
        return (Integer) writeSqlSession.selectOne(queryID, param);
    }


}