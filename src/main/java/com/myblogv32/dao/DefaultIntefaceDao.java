package com.myblogv32.dao;

import java.util.List;
import java.util.Map;

public interface DefaultIntefaceDao {
    public int insert(String queryID, Map<String, Object> param) throws Exception;

    public String insertStr(String queryID, Map<String, Object> param) throws Exception;

    public int insertObject(String queryID, Map<String, Object> param) throws Exception;

    public int insertSeqSelect(String queryID, Map<String, Object> param) throws Exception;

    public void insertNoReturn(String queryID, Map<String, Object> param) throws Exception;

    public int insertNoException(String queryID, Map<String, Object> param) throws Exception;

    public Map<String, Object> select(String queryID, Map<String, Object> param) throws Exception;

    public Object select(String queryID, Object param) throws Exception;

    public String selectStr(String queryID, Object param) throws Exception;

    public String selectWriteStr(String queryID, Object param) throws Exception;

    public List<Map<String, Object>> selectList(String queryID, Map<String, Object> param) throws Exception;

    public List<Map<String, Object>> selectList(String queryID, String param) throws Exception;

    public List<Map<String, Object>> selectList(String queryID, int param) throws Exception;

    public int selectCount(String queryID, Map<String, Object> param) throws Exception;

    public Map<String, Object> selectWithException(String queryID, Map<String, Object> param) throws Exception;

    public int update(String queryID, Map<String, Object> param) throws Exception;

    public int delete(String queryID, Map<String, Object> param) throws Exception;

    public int deleteNoException(String queryID, Map<String, Object> param) throws Exception;
}
