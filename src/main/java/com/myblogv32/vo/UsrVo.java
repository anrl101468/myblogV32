package com.myblogv32.vo;

import lombok.Data;

@Data
public class UsrVo {

    // 유저의 idx 고유값
    private int usrIdx;
    // 유저의 아아디
    private String usrId;
    // 유저의 비밀번호
    private String usrPassword;
    // 유저의 이름
    private String usrName;
    // 유저의 가입IP
    private String usrRegIp;
    // 유저의 가입날짜 및 시간
    private String usrRegDt;
    // 유저의 최근접속IP
    private String usrRecentIp;
    // 유저의 최근접속 날짜 및 시간
    private String usrRecentDt;
    // 유저의 비밀번호 틀린 횟수
    private int usrPwFailCnt;
    // 유저의 소개
    private String usrGt;
    // 회원 권한
    private String usrRole;
    // 방문자수
    private String uvtCnt;

    public UsrVo(int usrIdx, String usrId, String usrPassword, String usrName, String usrRegIp, String usrRegDt, String usrRecentIp, String usrRecentDt, int usrPwFailCnt, String usrGt, String usrRole) {
        this.usrIdx = usrIdx;
        this.usrId = usrId;
        this.usrPassword = usrPassword;
        this.usrName = usrName;
        this.usrRegIp = usrRegIp;
        this.usrRegDt = usrRegDt;
        this.usrRecentIp = usrRecentIp;
        this.usrRecentDt = usrRecentDt;
        this.usrPwFailCnt = usrPwFailCnt;
        this.usrGt = usrGt;
        this.usrRole = usrRole;
    }

    public UsrVo() {
    }

    public UsrVo(int usrIdx, String usrId, String usrPassword, String usrName, String usrRegIp, String usrRegDt, String usrRecentIp, String usrRecentDt, int usrPwFailCnt) {
        this.usrIdx = usrIdx;
        this.usrId = usrId;
        this.usrPassword = usrPassword;
        this.usrName = usrName;
        this.usrRegIp = usrRegIp;
        this.usrRegDt = usrRegDt;
        this.usrRecentIp = usrRecentIp;
        this.usrRecentDt = usrRecentDt;
        this.usrPwFailCnt = usrPwFailCnt;
    }

    public UsrVo(String usrId, String usrPassword, String usrName, String usrRegIp, String usrRegDt, String usrRecentIp, String usrRecentDt, int usrPwFailCnt) {
        this.usrId = usrId;
        this.usrPassword = usrPassword;
        this.usrName = usrName;
        this.usrRegIp = usrRegIp;
        this.usrRegDt = usrRegDt;
        this.usrRecentIp = usrRecentIp;
        this.usrRecentDt = usrRecentDt;
        this.usrPwFailCnt = usrPwFailCnt;
    }

    public UsrVo(String usrId, String usrName, String usrPassword) {
        this.usrId = usrId;
        this.usrPassword = usrPassword;
        this.usrName = usrName;
    }

    public UsrVo(int usrIdx, String usrId, String usrPassword, String usrName, String usrRegIp, String usrRegDt, String usrRecentIp, String usrRecentDt, int usrPwFailCnt, String usrRole) {
        this.usrIdx = usrIdx;
        this.usrId = usrId;
        this.usrPassword = usrPassword;
        this.usrName = usrName;
        this.usrRegIp = usrRegIp;
        this.usrRegDt = usrRegDt;
        this.usrRecentIp = usrRecentIp;
        this.usrRecentDt = usrRecentDt;
        this.usrPwFailCnt = usrPwFailCnt;
        this.usrRole = usrRole;
    }
}
