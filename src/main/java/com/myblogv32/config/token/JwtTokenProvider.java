package com.myblogv32.config.token;


import com.myblogv32.config.auth.PrincipalDetails;
import com.myblogv32.service.refreshToken.RefreshTokenService;
import com.myblogv32.service.usr.UsrService;
import com.myblogv32.util.StringUtil;
import com.myblogv32.util.TransUtil;
import com.myblogv32.vo.UsrVo;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang.StringUtils.substring;

/**
 * @PackageName: JwtTokenProvider
 * @FileName : JwtTokenProvider.java
 * @Date : 2023-04-23 오전 9:20
 * @프로그램 설명 : JWT토큰 주입
 * @author : ksy
 */

@Component
public class JwtTokenProvider {

    @Value("${jwt.secret}")
    private String SECRET_KEY;

    private final SecretKey secretKey;

    final Logger logger = LoggerFactory.getLogger(this.getClass());


    // 7 * 24 * 60 * 60 * 1000;
    // 일 ,시간,분, 초,밀리세컨드
    public static final long ACESS_EXPIRATION_TIME = 1 * 1 * 30 * 60 * 1000;; // 3시간
    //public static final long ACESS_EXPIRATION_TIME = 1 * 1 * 1 * 60 * 1000; // 5분
    public static final long REFRESH_EXPIRATION_TIME = 7 * 24 * 60 * 60 * 1000; // 7일
    //public static final long REFRESH_EXPIRATION_TIME = 1 * 1 * 2 * 60 * 1000; // 10분

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private UsrService usrService;
    public JwtTokenProvider(@Value("${jwt.secret}") String SECRET_KEY) {
        this.secretKey = Keys.hmacShaKeyFor(Decoders.BASE64URL.decode(SECRET_KEY));
    }

    //인증 객체에서 사용자 이름을 추출하여 JWT 토큰을 생성합니다.
    public String createAcessToken(UsrVo usrVo)
    {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() +  ACESS_EXPIRATION_TIME);
        Map<String,Object> claimsMap = TransUtil.toMap(usrVo);

        return Jwts.builder().subject(usrVo.getUsrId()).issuedAt(now).claims(claimsMap).expiration(expiryDate).signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public String createRefreshToken(UsrVo usrVo)
    {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() +  REFRESH_EXPIRATION_TIME);
        Map<String,Object> claimsMap = TransUtil.toMap(usrVo);

        return Jwts.builder()
                .subject(usrVo.getUsrId())
                .issuedAt(now)
                .expiration(expiryDate)
                .claims(claimsMap).signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    // JWT 토큰의 유효성 검사
    public boolean validateToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String token = this.resolveAcessToken(request);
        String refreshTokenIdx = this.resolveRefreshToken(request);
        try {
            // 버전 업 jjwt:0.9.1 -> jjwt:0.12.5
            Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token); // jjwt:0.12.5
            response.setHeader("loginYn", "Y");
            //Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);  jjwt:0.9.1
            return true;
        } catch (ExpiredJwtException ex) {
            if(request.getHeader("usrId") != null && !"null".equals(request.getHeader("usrId")))
            {
                Map<String,Object> map = new HashMap<>();
                map.put("usrId",request.getHeader("usrId"));
                UsrVo usrVo = TransUtil.toVo(usrService.selectUsr(map),UsrVo.class) ;
                map.put("tokenIdx",refreshTokenIdx);
                map.put("usrVo",usrVo);
                map = refreshTokenService.selectToken(map);
                String refreshToken = map.get("token").toString();
                if (this.validateRefreshToken(refreshToken)) {
                    request.setAttribute("Authorization", "Bearer " + refreshToken);
                        String ACESSTOKEN = this.createAcessToken(usrVo);
                        request.setAttribute("Authorization", "Bearer " + ACESSTOKEN);
                        response.setHeader("Authorization", "Bearer " + ACESSTOKEN);
                        return true;
                } else {
                    // refresh token이 유효하지 않은 경우, access token 재발급 불가능
                    logger.debug("JWT refresh token이 만료되었습니다.");
                    return false;
                }
            }else{
                return false;
            }
        } catch (UnsupportedJwtException ex) {
            logger.debug("JWT 토큰은 지원되지 않습니다.");
            return false;
        } catch (MalformedJwtException ex) {
            logger.debug("JWT 토큰은 유효하지 않습니다.");
            return false;
        } catch (SignatureException ex) {
            logger.debug("JWT 서명이 잘못되었습니다.");
            return false;
        } catch (IllegalArgumentException ex) {
            logger.debug("JWT 클레임 문자열이 비어 있습니다.");
            return false;
        }

    }

    public boolean validateRefreshToken(String token) throws Exception {
        try {
            Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token);
            return true;
        } catch (ExpiredJwtException ex) {
            logger.debug("JWT REFRESH 토큰이 만료되었습니다.");
            return false;
        } catch (UnsupportedJwtException ex) {
            logger.debug("JWT REFRESH 토큰은 지원되지 않습니다.");
            return false;
        } catch (MalformedJwtException ex) {
            logger.debug("JWT REFRESH 토큰은 유효하지 않습니다.");
            return false;
        } catch (SignatureException ex) {
            logger.debug("JWT REFRESH 서명이 잘못되었습니다.");
        } catch (IllegalArgumentException ex) {
            logger.debug("JWT REFRESH 클레임 문자열이 비어 있습니다.");
            return false;
        }

        return false;
    }

    //  HTTP 요청 헤더에서 JWT 토큰을 추출
    public String resolveAcessToken(HttpServletRequest request)
    {
        if(request.getAttribute("Authorization") == null && !"undefined".equals(request.getHeader("Authorization")))
        {
            request.setAttribute("Authorization",request.getHeader("Authorization"));
        }
        String bearerToken = StringUtil.getString(request.getAttribute("Authorization"));
        if(bearerToken != null && bearerToken.startsWith("Bearer"))
        {
            return bearerToken.substring(7);
        }
        return null;
    }

    public String resolveRefreshToken(HttpServletRequest request)
    {
        String bearerToken = request.getHeader("tokenIdx");
        if(bearerToken != null)
        {
            return bearerToken;
        }
        return null;
    }

    //  JWT 토큰을 기반으로 인증 객체를 생성
    public Authentication getAuthentication(UsrVo usrVo,String token) {
        UserDetails userDetails = new PrincipalDetails(usrVo);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 추출된 JWT 토큰에서 사용자 이름을 반환
    public String getUsername(String token) {
        // 버전 업 jjwt:0.9.1 -> jjwt:0.12.5
        return (String) Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload().get("usrId"); // jjwt:0.12.5
        //return (String) Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().get("usrId"); jjwt:0.9.1
    }

    public UsrVo getUsrVo(HttpServletRequest request,HttpServletResponse response) throws Exception {
        if(this.validateToken(request,response))
        {
            String token = StringUtil.getString(request.getAttribute("Authorization")).substring(7);
            // 버전 업 jjwt:0.9.1 -> jjwt:0.12.5
            Map<String,Object> parseMap = TransUtil.toMap(Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload()); // jjwt:0.12.5
            // Map<String,Object> parseMap = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody(); jjwt:0.9.1
            parseMap.remove("exp");
            parseMap.remove("sub");
            parseMap.remove("iat");
            UsrVo usrVo =  TransUtil.toVo(parseMap,UsrVo.class);
            return usrVo;
        }
        return null;
    }

}
