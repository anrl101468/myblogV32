package com.myblogv32.config.token;

import com.myblogv32.vo.UsrVo;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;

/**
 * @PackageName: JwtAuthenticationFilter
 * @FileName : JwtAuthenticationFilter.java
 * @Date : 2023-04-23 오전 9:44
 * @프로그램 설명 : 요청 헤더에 있는 JWT 토큰을 검증하고, 인증 객체를 생성하여 SecurityContext에 저장
 * @author : ksy
 */

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserDetailsService userDetailsService;

    /*React 프론트에서 axios를 사용하여 요청을 보낼 때마다, 새로운 SecurityContext가 생성*/
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if(!Objects.equals(request.getServletPath(), "/api/login") && !Objects.equals(request.getServletPath(), "/api/usr/duplicateCheck"))
        {
            if(jwtTokenProvider.resolveAcessToken(request) != null)
            {
                try {
                        if(jwtTokenProvider.validateToken(request,response))
                        {
                            String token;
                            if(request.getAttribute("Authorization") != null)
                            {
                                token = request.getAttribute("Authorization").toString().substring(7);
                            }
                            else
                            {
                                token = jwtTokenProvider.resolveAcessToken(request);
                            }
                            UsrVo usrVo = jwtTokenProvider.getUsrVo(request,response);
                            Authentication auth = jwtTokenProvider.getAuthentication(usrVo,token);
                            SecurityContextHolder.getContext().setAuthentication(auth);
                            response.setHeader("loginYn", "Y");
                        }
                        else{

                        }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
        filterChain.doFilter(request,response);
    }
}
