package com.myblogv32.config.auth;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    /*BadCredentialException 비밀번호가 일치하지 않을 때 던지는 예외
      InternalAuthenticationServiceException 존재하지 않는 아이디일 때 던지는 예외
      AuthenticationCredentialNotFoundException 인증 요구가 거부됐을 때 던지는 예외
      LockedException 인증 거부 - 잠긴 계정
      DisabledException 인증 거부 - 계정 비활성화
      AccountExpiredException 인증 거부 - 계정 유효기간 만료
      CredentialExpiredException 인증 거부 - 비밀번호 유효기간 만료
    */

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String errorMessage = "";
        if(exception instanceof BadCredentialsException)
        {
            errorMessage = "아이디 또는 비밀번호가 일치하지 않습니다.";
        }
        else if(exception instanceof InternalAuthenticationServiceException)
        {
            errorMessage = "아이디 또는 비밀번호가 일치하지 않습니다.";
        }
        logger.warn("===============================================================");
        logger.warn("아이디 : "+request.getParameter("usrId")+" 로그인 실패");
        logger.warn(" 원인 : " + exception.getMessage());
        logger.warn("===============================================================");
        errorMessage = URLEncoder.encode(errorMessage, StandardCharsets.UTF_8);
        response.setHeader("loginFail",errorMessage);
    }
}
