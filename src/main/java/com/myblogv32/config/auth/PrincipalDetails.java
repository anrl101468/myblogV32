package com.myblogv32.config.auth;

import com.myblogv32.vo.UsrVo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @PackageName: PrincipalDetails
 * @FileName : PrincipalDetails.java
 * @Date : 2023-04-22 오후 2:04
 * @프로그램 설명 : UserDetails를 받아서 필요의 맞게 재정의
 * @author : ksy
 */

// 시큐리티가 /login 주소 요청이 오면 낚아채서 로그인을 진행한다.
// 로그인 진행이 완료가 되면 시큐리티의 session을 만들어 줌 (Security ContextHolder 이 키값으로 스프링 시큐리티 session을 만듬)
// session의 들어갈수 있는 인자의 타입이 정해져 있음. (Authentication 객체)
// Authentication 안에 User정보가 있어야됨 -> Authentication도 인자값의 타입이 UserDetails타입으로 정해져 있음
// UserDetails객체는 PrincipalDetails 타입의 객체를 받게 정해져있음
// ex) new Security session(Authentication authentication)
// ex) new Authencation(UserDetails userDetails)
// Security session => Authentication => UserDetails => PrincipalDetails
public class PrincipalDetails implements UserDetails {

    private UsrVo usr;

    public PrincipalDetails(UsrVo usr) {
        this.usr = usr;
    }

    // 사용자가 가지고 있는 권한 정보를 반환합니다.
    // 반환 타입은 Collection<? extends GrantedAuthority>로,
    // 권한 정보를 나타내는 GrantedAuthority 객체들의 컬렉션을 반환합니다.
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collect = new ArrayList<>();
        String role = this.usr.getUsrRole();
        collect.add(new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return role;
            }
        });

        return collect;
    }

    // 사용자의 암호화된 패스워드를 반환합니다.
    @Override
    public String getPassword() {
        return usr.getUsrPassword();
    }

    // 사용자의 이름을 반환합니다.
    @Override
    public String getUsername() {
        return usr.getUsrId();
    }

    // 사용자 계정이 만료되었는지 여부를 반환합니다. 계정이 만료되었다면 false, 만료되지 않았다면 true를 반환합니다.
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // 사용자 계정이 잠겨있는지 여부를 반환합니다. 계정이 잠겨있다면 false, 잠겨있지 않다면 true를 반환합니다.
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 사용자의 자격증명(암호화된 패스워드)이 만료되었는지 여부를 반환합니다. 자격증명이 만료되었다면 false, 만료되지 않았다면 true를 반환합니다.
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 사용자 계정이 활성화되어 있는지 여부를 반환합니다. 계정이 활성화되어 있다면 true, 비활성화되어 있다면 false를 반환합니다.
    @Override
    public boolean isEnabled() {
        return true;
    }

    public UsrVo getUsr()
    {
        return this.usr;
    }
}
