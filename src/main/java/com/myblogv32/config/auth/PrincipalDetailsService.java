package com.myblogv32.config.auth;

import com.myblogv32.service.usr.UsrService;
import com.myblogv32.util.TransUtil;
import com.myblogv32.vo.UsrVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @PackageName: PrincipalDetailsService
 * @FileName : PrincipalDetailsService.java
 * @Date : 2023-04-22 오후 4:14
 * @프로그램 설명 : 로그인 과정
 * @author : ksy
 */
// 동작 방식
// 사용자로부터 통신 요청이 오면 dispacherServlet이 그에 맞는 컨트롤러나 기타 설정에 맞는곳으로 보내줌
// 스프링 시큐리티 설정에서 loginProcessingUrl로 설정되어 있는 url "/api/login" 으로 사용자가 요청을 보냈다면
// Ioc컨테이너에 등록되어 있는 UserDetailsService 타입의 객체를 찾아서 loadUserByUsername 메소드를 실행 시킴
@Service
public class PrincipalDetailsService implements UserDetailsService {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UsrService usrService;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        logger.info("--------------로그인--------------");
        Map<String,Object> usrMap = new HashMap<>();
        usrMap.put("usrId",userId);
        try {
            usrMap =  usrService.selectUsr(usrMap);
            logger.info("--------------결과--------------");
            if(usrMap != null)
            {
                UsrVo usrVo = TransUtil.toVo(usrMap,UsrVo.class);
                logger.info("--------------"+usrVo+"--------------");
                return new PrincipalDetails(usrVo);
            }
            else
            {
                logger.info("--------------실패--------------");
                logger.info("--------------"+userId+"가 존재하지 않습니다.--------------");
                throw new UsernameNotFoundException(userId + " not found");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
