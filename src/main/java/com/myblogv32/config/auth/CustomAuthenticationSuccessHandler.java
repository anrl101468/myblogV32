package com.myblogv32.config.auth;

import com.myblogv32.config.token.JwtTokenProvider;
import com.myblogv32.service.refreshToken.RefreshTokenService;
import com.myblogv32.service.usr.UsrService;
import com.myblogv32.util.DateUtil;
import com.myblogv32.vo.UsrVo;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @PackageName: CustomAuthenticationSuccessHandler
 * @FileName : CustomAuthenticationSuccessHandler.java
 * @Date : 2023-04-24 오전 7:14
 * @프로그램 설명 : 로그인의 성공했을때 추가 프로세스
 * @author : ksy
 */
@Service
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    final Log logger = LogFactory.getLog(this.getClass());


    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    UsrService usrService;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        String now = DateUtil.getLocalDateTime();

        PrincipalDetails userPrincipal = (PrincipalDetails) authentication.getPrincipal();

        UsrVo usrVo;
        // 로그인 시간 업데이트,비밀번호 틀린횟수 초기화
        usrVo = userPrincipal.getUsr();

        Map<String,Object> usrMap = new HashMap<>();
        usrMap.put("usrIdx",usrVo.getUsrIdx());
        usrMap.put("usrRecentIp",request.getHeader("ip"));
        usrMap.put("usrRecentDt",now);
        usrMap.put("usrPwFailCnt",0);

        String jwtAcessToken = jwtTokenProvider.createAcessToken(usrVo);
        response.setHeader("Authorization", "Bearer " + jwtAcessToken);
        String jwtRefreshToken = jwtTokenProvider.createRefreshToken(usrVo);

        Map<String,Object> tokenMap = new HashMap<>();
        tokenMap.put("usrVo",usrVo);
        tokenMap.put("token",jwtRefreshToken);
        try {
            int tokenIdx = refreshTokenService.createToken(tokenMap);
            response.setHeader("tokenIdx", tokenMap.get("tokenIdx").toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            usrService.updateUsr(usrMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        logger.info("===========================로그인 정보===========================");
        logger.info("아이디 :" + usrVo.getUsrId());
        logger.info("이름 :" + usrVo.getUsrName());
        logger.info("로그인 일시 :" +  usrVo.getUsrRecentDt());
        logger.info("로그인 아이피 :" +  usrVo.getUsrRecentIp());
        logger.info("===========================로그인 정보===========================");


    }

}