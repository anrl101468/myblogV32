package com.myblogv32.config.web;

import com.myblogv32.config.interceptor.WebAccessInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.List;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    WebAccessInterceptor webAccessInterceptor;

    @Autowired
    @Qualifier(value = "loggingIntercoptor")
    private HandlerInterceptor loggingInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(webAccessInterceptor);
        registry.addInterceptor(loggingInterceptor);
    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS").allowedHeaders("*");
    }

    /*@Bean
    public ViewResolver viewJspResolver() {
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
        internalResourceViewResolver.setPrefix("WEB-INF/views/");
        internalResourceViewResolver.setSuffix(".jsp");
        internalResourceViewResolver.setViewClass(JstlView.class);
        internalResourceViewResolver.setOrder(1);
        return internalResourceViewResolver;
    }*/


   /*  viewJspResolver 으로 변경
   @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/views/");
        viewResolver.setSuffix(".jsp");
        registry.viewResolver(viewResolver);
        WebMvcConfigurer.super.configureViewResolvers(registry);
    }*/
   @Bean
   public BCryptPasswordEncoder bCryptPasswordEncoder()
   {
       return new BCryptPasswordEncoder();
   }

    public BeanNameViewResolver downloadViewResolver(){
        BeanNameViewResolver downloadViewResolver = new BeanNameViewResolver();
        downloadViewResolver.setOrder(0);
        return downloadViewResolver;
    }

    //MappingJacksonJsonView 설정
    @Bean
    public MappingJackson2JsonView jsonView(){
        return new MappingJackson2JsonView();
    }

    @Bean
    public MappingJackson2HttpMessageConverter jacksonMessageConverter(){
        return new MappingJackson2HttpMessageConverter();
    }

    @Bean
    public Jaxb2RootElementHttpMessageConverter messageConverter(){
        return new Jaxb2RootElementHttpMessageConverter();
    }

   /* @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        WebMvcConfigurer.super.configureMessageConverters(converters);
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
       converters.add(messageConverter());
       converters.add(jacksonMessageConverter());
        WebMvcConfigurer.super.extendMessageConverters(converters);
    }*/

}
