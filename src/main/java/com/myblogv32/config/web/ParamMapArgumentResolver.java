package com.myblogv32.config.web;

import com.myblogv32.config.token.JwtTokenProvider;
import com.myblogv32.util.DateUtil;
import com.myblogv32.util.StringUtil;
import com.myblogv32.util.common.RequestParams;
import com.myblogv32.vo.UsrVo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.catalina.core.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.support.WebArgumentResolver.UNRESOLVED;

//HTTP request 객체에 있는 파라미터이름과 값을 commandMap에 담는다.
//todoDel 삭제 예정


public class ParamMapArgumentResolver implements HandlerMethodArgumentResolver {

    final Log logger = LogFactory.getLog(getClass());

    JwtTokenProvider jwtTokenProvider;

    public ParamMapArgumentResolver(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    /**
     * Controller의 메소드 argument에 paramMap Map 객체가 있다면
     * HTTP request 객체에 있는 파라미터이름과 값을 paramMap 담아 returng한다.
     * 배열인 파라미터 값은 배열로 Map에 저장한다.
     */
    //@ParamFilter
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(RequestParams.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        Class<?> clazz = methodParameter.getParameterType();
        String paramName = methodParameter.getParameterName();
        HttpServletRequest request = (HttpServletRequest)webRequest.getNativeRequest();
        HttpServletResponse response = (HttpServletResponse)webRequest.getNativeResponse();

        boolean isSend = false;

        //if(clazz.equals(Map.class) && "paramMap".equals(paramName)) {
        if(clazz.equals(Map.class)) {

            Enumeration<?> enumeration = request.getParameterNames();
            while(enumeration.hasMoreElements()) {
                String key = (String) enumeration.nextElement();
                String[] values = request.getParameterValues(key);
                if(values != null) {
                    paramMap.put(key, (values.length > 1) ? values : StringUtil.setInjectionReplace(values[0]) );
                }
            }
            if(!"undefined".equals(request.getHeader("Authorization")) && jwtTokenProvider.validateToken(request,response))
            {
                UsrVo usrVo = jwtTokenProvider.getUsrVo(request,response);
                paramMap.put("usrVo",usrVo);
            }
            isSend = true;
        }
        if(isSend){
            paramMap.put("ACCESS_IP", request.getHeader("ip"));
            paramMap.put("ACCESS_DT", DateUtil.getLocalDateTime());
            return paramMap;
        } else {
            return UNRESOLVED;
        }
    }
}
