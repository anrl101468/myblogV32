package com.myblogv32.config.web;

import com.myblogv32.config.token.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.context.request.async.DeferredResultProcessingInterceptor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* @PackageName: com.config
* @FileName :  CustomRequestMappingHandlerAdapter.java
* @Date : 2021. 12. 9.
* @프로그램 설명 : CommandMapArgumentResolver 핸들러를 RequestMappingHandlerAdapter 등록시키는 Class
* @author KSY
*/
@Configuration
public class CustomRequestMappingHandlerAdapter extends RequestMappingHandlerAdapter {

    @Autowired
    MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    Jaxb2RootElementHttpMessageConverter messageConverter;

    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Override
    public void setMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        messageConverters.add(jacksonMessageConverter);
        messageConverters.add(messageConverter);
        super.setMessageConverters(messageConverters);
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();

        if (getArgumentResolvers() != null) {
            List<HandlerMethodArgumentResolver> list = getArgumentResolvers();
            List<HandlerMethodArgumentResolver> customResolvers = new ArrayList<HandlerMethodArgumentResolver>();

            customResolvers.addAll(list);
            customResolvers.add(19, new ParamMapArgumentResolver(jwtTokenProvider));
            setArgumentResolvers(customResolvers);
        }

        List<HttpMessageConverter<?>> messageConverters = getMessageConverters();
        messageConverters.add(jacksonMessageConverter);
        messageConverters.add(messageConverter);

        List<HandlerMethodReturnValueHandler> handle =getReturnValueHandlers();
        List<HandlerMethodReturnValueHandler> customHandle = new ArrayList<HandlerMethodReturnValueHandler>();
        customHandle.addAll(handle);
        /*
        CustomProcess requestResponseBodyMethodProcessor = (CustomProcess) handle.get(11);
        requestResponseBodyMethodProcessor.setMessage(messageConverters);
        */
        Objects.requireNonNull(handle).get(11);
        CustomProcess custom = new CustomProcess(messageConverters);
        customHandle.add(7,custom);
        customHandle.remove(12);
        setReturnValueHandlers(customHandle);


        System.out.print(handle);
/*        List<HttpMessageConverter<?>> messageConverters = getMessageConverters();
        messageConverters.add(jacksonMessageConverter);
        messageConverters.add(messageConverter);

        setMessageConverters(messageConverters);*/
        //System.out.println(messageConverters);
    }


}
    /*@Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();

        if(getArgumentResolvers() != null) {
            List<HandlerMethodArgumentResolver> list = getArgumentResolvers();
            List<HandlerMethodArgumentResolver> customResolvers = new ArrayList<HandlerMethodArgumentResolver>();

            System.out.println("===============================================");
            System.out.println(getArgumentResolvers());
            System.out.println("===============================================");
            MapMethodProcessor mapMethodProcessor = null;

            for(HandlerMethodArgumentResolver resolver : list){
                if(resolver instanceof MapMethodProcessor){
                    mapMethodProcessor = (MapMethodProcessor) resolver;
                }else {
                    customResolvers.add(resolver);
                }
            }
            if(mapMethodProcessor != null){
                customResolvers.add(mapMethodProcessor);
            }
            setArgumentResolvers(customResolvers);
        }
    }*/


