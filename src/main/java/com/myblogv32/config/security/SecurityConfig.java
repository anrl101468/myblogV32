package com.myblogv32.config.security;

import com.myblogv32.config.auth.CustomAuthenticationFailureHandler;
import com.myblogv32.config.auth.CustomAuthenticationSuccessHandler;
import com.myblogv32.config.token.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @PackageName: SecurityConfig
 * @FileName : SecurityConfig.java
 * @Date : 2023-04-22 오후 1:47
 * @프로그램 설명 : 스프링 시큐리티 기본 설정
 * @author : ksy
 */
@EnableWebSecurity // 활성화시 스프링 시큐리티 필터가 스프링 필터체인에 등록
@Configuration
public class SecurityConfig {

    // Bean 어노테이션을 사용하면 해당 메서드의 리턴되는 오브젝트를 Ioc컨테이너에 등록함


    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Autowired
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @Bean
    public SecurityFilterChain UserFilterChain(HttpSecurity http) throws Exception
    {
        http.authorizeHttpRequests( authorize -> authorize
                .anyRequest().permitAll()
            )
        .csrf(
                csrf -> {
                    try {
                        csrf
                                .disable()
                                .sessionManagement(
                                        sessionManagement ->
                                                sessionManagement
                                                        .sessionCreationPolicy( SessionCreationPolicy.STATELESS)
                                                        .sessionConcurrency(sessionConcurrency ->
                                                                sessionConcurrency
                                                                    .maximumSessions(1)
                                                                )
                                );
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
        )
        .cors( cors ->
                cors.configurationSource(corsConfigurationSource())
        )
        .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
        .formLogin(
                formLogin ->
                        formLogin
                                .usernameParameter("usrId")
                                .passwordParameter("usrPassword")
                                .loginProcessingUrl("/api/login")
                                .successHandler(customAuthenticationSuccessHandler)
                                .failureHandler(customAuthenticationFailureHandler)
        )
        .logout(
                logout ->
                        logout.logoutUrl("/api/logout")
        );

        return http.build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOriginPattern("*");
        //configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


}
