package com.myblogv32.config.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.BooleanTypeHandler;
import org.apache.ibatis.type.DateTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Configuration
public class DataSourceConfig{

    @Bean(name="myblogv32")
    @ConfigurationProperties(prefix="spring.datasource.hikari")
    public HikariConfig hikariConfig() {
        return new HikariConfig();
    }

    @Primary
    @Bean(name = "dataSource")
    public DataSource dataSource() throws Exception {
        Map<Object, Object> targetSource = new HashMap<>();
        targetSource.put("myblogv32", new HikariDataSource(hikariConfig()));

        RoutingDataSource dataSource = new RoutingDataSource();
        dataSource.setTargetDataSources(targetSource);

        return dataSource;
    }

    @Primary
    @Bean(name = "sessionFactory")
    public SqlSessionFactory firstSqlSessionFactory(@Qualifier("dataSource") DataSource firstDataSource, ApplicationContext applicationContext) throws Exception {

        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(firstDataSource);

        Resource[] res = new PathMatchingResourcePatternResolver().getResources("classpath*:/sql/**/*.xml");

        sessionFactory.setMapperLocations(res);

        sessionFactory.setTypeAliasesPackage("com/myblogv32/vo");

        Objects.requireNonNull(sessionFactory.getObject()).getConfiguration().setMapUnderscoreToCamelCase(true);

        sessionFactory.setTypeHandlers(new TypeHandler[]{new DateTypeHandler(), new BooleanTypeHandler()});


        return sessionFactory.getObject();
    }

    @Primary
    @Bean(name = "writeSqlSession")
    public SqlSessionTemplate writeSqlSession(@Qualifier("sessionFactory")SqlSessionFactory firstSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(firstSqlSessionFactory);
    }

    @Primary
    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager(@Autowired @Qualifier("dataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name="readSqlSession")
    public SqlSessionTemplate readSqlSession(@Qualifier("sessionFactory")SqlSessionFactory secondSqlSessionFactory) throws Exception{
        return new SqlSessionTemplate(secondSqlSessionFactory);
    }

}

