package com.myblogv32.config.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class LoggingIntercoptor implements HandlerInterceptor {
    public final static Logger logger =LoggerFactory.getLogger(LoggingIntercoptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUri = request.getRequestURI().toString();
        Class<?> thisClass = handler.getClass();
        logger.info("=====================================================================================\n\n");
        logger.info("::: " + thisClass + " ["+requestUri + "] Start :::");
        logger.info("::: 접속 아이피 "   + request.getHeader("ip")+" :::");
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        String requestUri = request.getRequestURI().toString();
        Class<?> thisClass = handler.getClass();
        logger.info("::: " + thisClass + " ["+requestUri+"] End :::");
        logger.info("=====================================================================================\n\n");
    }
}
