/**
 * 
 */
package com.myblogv32.util.common;

import java.lang.annotation.*;

@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestParams {
	String value() default "";
}
