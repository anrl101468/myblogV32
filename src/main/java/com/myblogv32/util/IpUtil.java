package com.myblogv32.util;

import jakarta.servlet.http.HttpServletRequest;

import java.net.InetAddress;

public class IpUtil {

    public static String getClientIpAddress(HttpServletRequest request) throws Exception{
        String ipAddress = request.getRemoteAddr();

        if(ipAddress != null && InetAddress.getByName(ipAddress).isLoopbackAddress()) {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        }

        return ipAddress;
    }
}