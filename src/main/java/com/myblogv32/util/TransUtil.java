package com.myblogv32.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * @PackageName: TransUtil
 * @FileName : TransUtil.java
 * @Date : 2023-04-22 오후 1:30
 * @프로그램 설명 : vo 및 map 변환
 * @author : ksy
 */
public class TransUtil {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> Map<String,Object> toMap(T target) {
        return objectMapper.convertValue(target, new TypeReference<Map<String, Object>>() {});
    }

    public static <T> T toVo(Map<String,Object> map,Class<T> targetClass)
    {
        return objectMapper.convertValue(map,targetClass);
    }
}
