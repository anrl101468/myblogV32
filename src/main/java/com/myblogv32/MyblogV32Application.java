package com.myblogv32;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyblogV32Application {

    public static void main(String[] args) {
        SpringApplication.run(MyblogV32Application.class, args);
    }

}
