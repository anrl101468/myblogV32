CREATE TABLE `T_USR` (
                         `USR_IDX` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '고유 회원번호',
                         `USR_ID` varchar(100) NOT NULL COMMENT '회원 아이디',
                         `USR_PASSWORD` varchar(100) NOT NULL COMMENT '회원 비밀번호',
                         `USR_NAME` varchar(100) NOT NULL COMMENT '회원 이름',
                         `USR_REG_IP` varchar(100) NOT NULL COMMENT '회원 가입아이피',
                         `USR_REG_DT` varchar(200) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '회원 가입날짜',
                         `USR_RECENT_IP` varchar(100) NOT NULL COMMENT '회원 최근 로그인아이피',
                         `USR_RECENT_DT` varchar(200) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '회원 최근 로그인날짜',
                         `USR_PW_FAIL_CNT` int(11) NOT NULL COMMENT '회원 로그인 실패 횟수(5회 이상 잠김)',
                         `USR_ROLE` varchar(20) NOT NULL COMMENT '회원 권한',
                         `USR_GT` varchar(5000) DEFAULT NULL COMMENT '회원 소개(greetings)',
                         PRIMARY KEY (`USR_IDX`),
                         UNIQUE KEY `USR_ID` (`USR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `T_MENU` (
                          `MENU_IDX` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '메뉴 고유번호',
                          `USR_IDX` bigint(20) NOT NULL COMMENT '회원 고유번호',
                          `MENU_UP_IDX` bigint(20) NOT NULL COMMENT '상위 메뉴 고유번호',
                          `MENU_NAME` varchar(100) NOT NULL COMMENT '메뉴 명',
                          `MENU_REG_IP` varchar(100) NOT NULL COMMENT '메뉴 저장 아이피',
                          `MENU_REG_DT` varchar(100) NOT NULL COMMENT '메뉴 저장일시',
                          `MENU_MOD_IP` varchar(100) NOT NULL COMMENT '메뉴 수정 아이피',
                          `MENU_MOD_DT` varchar(100) NOT NULL COMMENT '메뉴 수정일시',
                          PRIMARY KEY (`MENU_IDX`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4

CREATE TABLE `T_UPL` (
                         `UPL_IDX` bigint NOT NULL AUTO_INCREMENT COMMENT '게시물 고유번호',
                         `MENU_IDX` varchar(25) NOT NULL COMMENT '메뉴 번호',
                         `USR_IDX` varchar(40) NOT NULL COMMENT '유저 고유 idx',
                         `UPL_TITLE` varchar(500) NOT NULL COMMENT '게시글 제목',
                         `UPL_CONTENT` varchar(5000) NOT NULL COMMENT '게시글 내용',
                         `UPL_REG_IP` varchar(100) NOT NULL COMMENT '게시물 저장 아이피',
                         `UPL_REG_DT` varchar(100) NOT NULL COMMENT '게시물 저장 일시',
                         `UPL_MOD_IP` varchar(100) NOT NULL COMMENT '게시물 수정 아이피',
                         `UPL_MOD_DT` varchar(100) NOT NULL COMMENT '게시물 수정 일시',
                         PRIMARY KEY (`UPL_IDX`),
                         KEY `t_upl_where` (`UPL_REG_DT` DESC,`UPL_IDX` DESC,`MENU_IDX`) USING BTREE,
                         KEY `t_upl_where2` (`UPL_REG_DT` DESC,`UPL_IDX` DESC,`MENU_IDX`,`UPL_TITLE`) USING BTREE,
                         KEY `t_upl_where3` (`UPL_REG_DT` DESC,`UPL_IDX` DESC,`MENU_IDX`,`UPL_TITLE`,`USR_IDX`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1376221 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `T_UCL` (
                         `UCL_IDX` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '댓글 고유번호',
                         `UPL_IDX` bigint(20) DEFAULT NULL COMMENT '게시물 고유번호',
                         `UCL_CONTENT` varchar(5000) DEFAULT NULL COMMENT '댓글 내용',
                         `UCL_REG_IP` varchar(100) DEFAULT NULL COMMENT '댓글 저장 아이피',
                         `UCL_REG_DT` varchar(200) DEFAULT NULL COMMENT '댓글 저장일시',
                         `UCL_MOD_IP` varchar(100) DEFAULT NULL COMMENT '댓글 수정 아이피',
                         `UCL_MOD_DT` timestamp NULL DEFAULT NULL COMMENT '댓글 수정 일시',
                         `UCL_NICKNAME` varchar(100) NOT NULL COMMENT '댓글 저장 아이디',
                         `UCL_PW` varchar(100) NOT NULL COMMENT '댓글 저장 비밀번호',
                         PRIMARY KEY (`UCL_IDX`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='댓글 테이블'

CREATE TABLE `T_UVT_CNT` (
                             `USR_IDX` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '유저 고유번호',
                             `UVT_DT` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '날짜',
                             `UVT_CNT` int DEFAULT '0' COMMENT '방문자수',
                             PRIMARY KEY (`USR_IDX`,`UVT_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `T_PV_CNT` (
                            `UPL_IDX` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '글 고유번호',
                            `PV_DT` varchar(100) NOT NULL COMMENT '날짜',
                            `PV_CNT` int DEFAULT '0' COMMENT '포스팅 글 조회수',
                            PRIMARY KEY (`UPL_IDX`,`PV_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `T_CON_HIS` (
                             `ACCESS_IP` varchar(100) NOT NULL COMMENT '접근 아이피',
                             `ACCESS_DT` varchar(100) NOT NULL COMMENT '접근 일시'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `REFRESH_TOKEN` (
                                 `TOKEN_IDX` int NOT NULL AUTO_INCREMENT,
                                 `TOKEN` varchar(5000) NOT NULL,
                                 PRIMARY KEY (`TOKEN_IDX`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci